# Challenge Ansible : Configuration automatique de Nginx et déploiement d'un site web statique

Le défi consiste à créer une collection Ansible qui automatise la configuration d'un serveur web Nginx et le déploiement d'un site web statique sur un serveur distant. Voici les étapes à suivre pour relever ce défi :

## Création de la Collection web_server :
- Utilisez la commande `ansible-galaxy collection init mon_namespace.web_server` pour initialiser une nouvelle collection sous votre propre namespace.
- La collection doit inclure deux rôles distincts : nginx et static_site.

## Configuration du rôle nginx :
- Créez un rôle dans votre collection pour installer et configurer Nginx.
- Le rôle doit comporter des tâches pour installer Nginx, configurer un site virtuel par défaut, et s'assurer que le service Nginx est actif et fonctionne correctement.

## Déploiement du rôle static_site :
- Écrivez un rôle pour déployer un site web statique.
- Ce rôle doit inclure des tâches pour transférer des fichiers HTML statiques dans le répertoire racine du serveur web Nginx.
- Assurez-vous que les fichiers statiques sont correctement servis par Nginx.

## Création d'un Playbook deploy_static_site.yml :
- Développez un playbook principal qui utilise les deux rôles (nginx et static_site) pour configurer un serveur Nginx et déployer un site web statique.
- Le playbook doit orchestrer l'installation et la configuration de manière séquentielle, en s'assurant que Nginx est configuré pour servir le contenu statique.

## Utilisation d'Ansible Vault pour chiffrer l'inventory :
- Utilisez Ansible Vault pour chiffrer l'inventory contenant les informations sensibles telles que les adresses IP, les noms d'utilisateur, les mots de passe SSH, etc.
commandes : 
1. `ansible-vault encrypt inventory.yml` (pour chiffrer l'inventory)
2. `ansible-vault edit inventory.yml` (pour modifier l'inventory chiffré)
3. `ansible-playbook -i inventory.yml playbook.yml --ask-vault-pass`(pour exécuter le playbook)
4. `ansible-vault decrypt inventory.yml` (pour déchriffrer l'inventory)

- Assurez-vous que seuls les utilisateurs autorisés ont accès aux données sensibles enregistrées dans l'inventory chiffré.

Enjoy ! 🚀
